require 'mock-builder/cmd_runner'
require 'mock-builder/podman_inspect'
require 'etc'

module MockBuilder
  # Definition for building Mock Container Image.
  class ContainerImage
    include CMDRunner
    include PodmanInspect

    attr_reader :image

    # Create a new instance for Container Image
    # @param fedora_version String Container version used for the source image (e.g. latest, f38, ...)
    # @param source_image String Source image to use as the base for the mock container. (Currently unused)
    def initialize(fedora_version = 'latest', source_image: 'registry.fedoraproject.org/fedora')
      @fedora_version = fedora_version

      @source_image = source_image + ":#{@fedora_version}"

      @image = "mock_builder:#{@fedora_version}"
    end

    # Build the image using podman
    # By default it will use the currently logged in user and use it in the container.
    # That's solely for the directories to be mapped OK as the resulting container is rootless.
    def build
      # Don't rebuild the image if it already is created
      # TODO: Force refresh the image or update.
      return self if self.exists?

      workdir = File.expand_path(__dir__)
      container_file = File.join(workdir, 'container', 'Containerfile')

      status = execute_quiet(
        'podman', 'build', workdir,
        '-t', @image,
        '-f', container_file,
        '--build-arg=FEDORA_VERSION=' + @fedora_version,
        '--build-arg=MOCK_RUN_USER=' + Etc.getlogin.to_s
      )

      if status > 0
        $stderr.puts "Failed to build base Mock image"
        exit 1
      end

      self
    end

    private

    def exists?
      if inspect_image(@image).empty?
        false
      else
        true
      end
    end
  end
end
