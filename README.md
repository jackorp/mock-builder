# mock-builder

`mock-builder` is an utility for spawning an RPM build in a container.

*Dependencies*

Ruby, `podman`.

Install on Fedora:
```console
$ sudo dnf install ruby podman rubygem-json
```

*Why?*

Mainly to work around the limit of a single concurrent build per chroot.
By isolating mock in a container, you can now spawn multiple isolated parallel builds
without needing to fiddle around with your mock root configs.

*How*

`mock-builder` uses podman to run `mock` isolated from the host.

Mock-builder first builds a base container image that will be used for any subsequent RPM build,
it will install `mock` and prepare the in-container user which will the be running the actual `mock`
command in the resulting container.

Then, when actually running the container, the mentioned built image will be used. The directory for
artifacts and the SRPM is mounted into the container as a volume to share these between the
container and the host.

## Usage

Simply run the command with the source RPM
```console
$ mock-build foo.src.rpm
```

This will build the base container and then use volume binding to provide the source RPM and also to bring the result back

By default the resulting artifacts can be found in `./results`

### Specifying mock's chroot

You can use `-r` or `--root` for default configs found in mock.
```console
$ mock-build -r fedora-38-x86_64 foo.src.rpm
```

### Specifying results directory

You can use `--results` to specify which directory will contain the results, this has to be a relative path
```console
$ mock-build --results ./my_resdir foo.src.rpm
```

The `--root` and `--results` options can be combined.

## Troubleshooting

### Mock fails on mount

If you see the following message, or similar, from mock
```
ERROR: Command failed:
 # /bin/mount -n -o remount,nodev,noexec,nosuid,readonly,rprivate,rbind --target /var/lib/mock/fedora-rawhide-x86_64-bootstrap/root/proc
```
That most likely means that SELinux is preventing successful execution.
By default I do not like `--privileged`, as it is, IMHO, the same as setting SELinux to permissive or turning it off completely. I only add capabilities it actually needs (CAP_SYS_ADMIN in this case).

So, while `--privileged` might "fix" this problem, it just works around it, by (this is just my theory) taking different paths for file ownerships and similar.

To actually *fix* the problem more appropriately:

There is a SELinux policy module source included you can compile and enable.

To compile and allow the SELinux policy run the following in the `selinux_policy` directory:
```console
$ cd selinux_policy
$ make -f /usr/share/selinux/devel/Makefile allow_mock_mounts.pp
$ sudo semodule -i allow_mock_mounts.pp
```

# LICENSE
MIT
```
Copyright (c) 2023 Jaroslav Prokop <jprokop@redhat.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
