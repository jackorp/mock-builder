require 'optparse'

module MockBuilder
  class RunOptions
    def initialize
      @options = {}
      @parser = OptionParser.new

      @parser.banner = 'Usage: mock-builder [-r <root>] [--results <dir>] src_rpm'

      @parser.on("-r", "--root CHROOT", "Mock chroot to use for build") do |r|
        @options[:root] = r
      end

      @parser.on("--results DIR", "Specifies where the resulting artifacts will be placed") do |d|
        @options[:results_dir] = d
      end

      @parser.on("-h", "--help", "Print this help") do
        puts @parser
        exit
      end

      @parser.parse!

      if ARGV.size != 1
        puts "Expected source RPM, but found #{ARGV.size ? ARGV.size - 1 : 0} additional arguments."
        exit 1
      end

      @options[:src_rpm] = ARGV[0]
    end

    def root
      @options[:root]
    end

    def results_dir
      @options[:results_dir]
    end

    def src_rpm
      @options[:src_rpm]
    end
  end
end
