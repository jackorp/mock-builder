require 'mock-builder/cmd_runner'
require 'json'

module MockBuilder
  # Mixin wrapper around Podman Inspect. Only methods we care about are implemented.
  module PodmanInspect
    include CMDRunner

    # Inspect the information about an image.
    # @param image String Image to inspect via podman.
    # @return JSON Returns the json object retreived by parsing the STDOUT of podman inspect.
    #              Return empty array if the command failed.
    def inspect_image(image)
      status, json_str = execute('podman', 'inspect', image.to_s)

      return [] if status != 0

      JSON.parse(json_str)
    end
  end
end
