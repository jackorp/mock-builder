require 'open3'

module MockBuilder
  # Simple wrapper around command execution.
  module CMDRunner
    # Execute command and capture the STDOUT. STDERR is untouched and is allowed to print to console.
    # @param *args String Command passed over to shell
    # @return [Integer, String] Return array of exit status and captured STDOUT respectively
    def execute(*args)
      stdout, s = Open3.capture2(*args)
      [s.exitstatus, stdout]
    end

    # Execute command and ignore STDOUT and STDERR
    # @param *args String Command passed over to shell
    # @return Integer Return numerical value of the exit code.
    def execute_quiet(*args)
      _, s = Open3.capture2e(*args)
      s.exitstatus
    end
  end
end
