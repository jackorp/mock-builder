# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'mock-builder'
  s.version = '0.1.0'
  s.authors = ['Jaroslav Prokop']
  s.email = ['jprokop@redhat.com']

  summary = 'MockBuilder is a wrapped mock and podman for parallel sandboxed builds.'
  s.summary = summary.clone
  s.description = summary.clone
  s.homepage = 'https://gitlab.com/jackorp/mock-builder'
  s.licenses = ['MIT']
  s.files = [
    'bin/mock-builder',
    'lib/mock-builder/container/Containerfile',
    'lib/mock-builder/options.rb',
    'lib/mock-builder/cmd_runner.rb',
    'lib/mock-builder/container_image.rb',
    'lib/mock-builder/mock_runner.rb',
    'lib/mock-builder/podman_inspect.rb',
    'lib/mock-builder.rb',
    'mock-builder.gemspec',
    'selinux_policy/allow_mock_mounts.te',
    'README.md',
    'LICENSE'
  ]
  s.bindir = 'bin'
  s.executables = 'mock-builder'
  s.require_paths = ['lib']
end
